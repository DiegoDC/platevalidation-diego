test("Enrollement Size", function (assert) {
    assert.equal(isValidPlate("3490WWW"), true, "Test1");
	assert.equal(isValidPlate("4321LLD"), true, "Test2");
	assert.equal(isValidPlate("5641JK"), false, "Test3");
	assert.equal(isValidPlate("5489AAA"), false, "Test4");
	assert.equal(isValidPlate("1594S"), false, "Test5");
	assert.equal(isValidPlate("1221FDG"), true, "Test6");
	assert.equal(isValidPlate("864ABDC"), false, "Test7");
	assert.equal(isValidPlate("3421AWdX"), false, "Test8");
	assert.equal(isValidPlate("123daaWW"), false, "Test9");
	assert.equal(isValidPlate("123NOOB"), false, "Test10");
	assert.equal(isValidPlate("6666LAG"), false, "Test11");
	assert.equal(isValidPlate("7912OWO"), false, "Test12");
	assert.equal(isValidPlate("93IwI"), false, "Test113");
	assert.equal(isValidPlate("1230lWl"), true, "Test14");
	assert.equal(isValidPlate("5688BBC"), true, "Test15");
	assert.equal(isValidPlate("1485DDC"), true, "Test16");
	assert.equal(isValidPlate("8888SDK"), true, "Test17");
	assert.equal(isValidPlate("8888OBSJ"), false, "Test18");
	assert.equal(isValidPlate("5214ALGD"), false, "Test19");
	assert.equal(isValidPlate("2934PKC"), true, "Test20");
	assert.equal(isValidPlate("8470DDD"), true, "Test21");
	assert.equal(isValidPlate("0005MEN"), false, "Test22");
	assert.equal(isValidPlate("5684KKK"), true, "Test23");
	assert.equal(isValidPlate("2458SSS"), true, "Test24");
	assert.equal(isValidPlate("8123CaC"), false, "Test24");
	assert.equal(isValidPlate("1230PDA"), false, "Test25");
});

test("Enrollement Q ENIE", function (assert) {
	assert.equal(isValidPlate("0157PCL"), true, "Test1");
	assert.equal(isValidPlate("7510MMM"), true, "Test2");
    assert.equal(isValidPlate("6789JJQ"), false, "Test3");
	assert.equal(isValidPlate("5874QQQ"), false, "Test4");
	assert.equal(isValidPlate("3560XDX"), true, "Test5");
	assert.equal(isValidPlate("5420DXD"), true, "Test6");
	assert.equal(isValidPlate("9999ÑJK"), false, "Test7");
	assert.equal(isValidPlate("7384QJK"), false, "Test8");
});


