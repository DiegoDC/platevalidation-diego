/*
 * Given the three sides of a triangle it calculates the type of the
 * triangle.
 */
function isValidPlate(plate) {
  var re = /(\d\d\d\d[BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ])/i;
  if(plate.match(re) != null) {
    gtag('event', 'click', { // eslint-disable-line
      'event_category' : 'validate',
      'event_label' : 'type'
    });
    return true;
  } else {
    return false;
  }
}
